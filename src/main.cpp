#include <iostream>
#include <fstream>
#include "sha1.h"
#include <string>
#include <ctype.h>

int main (int argc, char * const argv[]) {
	
	if(argc < 2 || argc > 3) {
		std::cout << "usage: [program name] [new password] [directory]\n";
		return 0;
	}
	
	std::string toHash(argv[1]);
	std::string filename = "program2patch2.dif"; 
	
	if(argc == 3) {
		std::string dir(argv[2]);
		if(dir.length() > 0 && argv[2][dir.length()-1] != '/')
			filename = dir + '/' + filename;
		else filename = dir + filename;
	}
	
	unsigned char temphash[20];
	char hexstring[41];
	sha1::calc(toHash.c_str(),toHash.length(), temphash); 
	sha1::toHexString(temphash, hexstring);
	
	std::ofstream outfile(filename.c_str());
	
	outfile << "49559107.program2.exe" << std::endl;
	std::string orig[20] = {"49: 7F ", "4A: A1 ", "4B: 67 ", "4C: 33 ", "4D: FC ", "4E: 36 ", "4F: 41 ", 
				"50: E5 ", "51: DF ", "52: DA ", "53: CC ", "54: 43 ", "55: 88 ", "56: CA ", "57: 21 ",
				"58: 5D ", "59: F6 ", "5A: 77 ", "5B: 1C ", "5C: 91 "};
	
	int i=0;
	for(int j=0; j<20; j++) {
		outfile << "00021C" + orig[j] << (char) toupper(hexstring[i]) << (char) toupper(hexstring[i+1]) << std::endl;
		i+=2;
	}

	outfile.close();
    
    return 0;
}
